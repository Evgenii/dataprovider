<?php

include_once 'testClassArray.php';

class B extends A
{
	protected $db = array();
	
	public function setProp($value, $name)
	{
		if (!is_null($value)) {
			$this->db[$name] = 'prefix_' . $value;
		} else {
			unset($this->db[$name]);
		}
	}
	
	public function getProp($name)
	{
		if (array_key_exists($name, $this->db)) {
			return $this->db[$name];
		}
		return 'n/a';
	}
	
	public function setPropDatas($value, $name)
	{
		if (!is_null($value)) {
			$this->_datas[$name] = 'pre_' . $value;
		} else {
			unset($this->_datas[$name]);
		}
	}
	
	public function getVarFromDatas($name)
	{
		if (array_key_exists($name, $this->_container)) {
			return $this->_container[$name];
		}
		return null;
	}
	
	public function getPropDatas($name)
	{
		$result = $this->getVarFromDatas($name);
		return (is_null($result)) ? 'non in _datas' : '[' . $result . ']';
	}
	
	public function setPropClosure($nameProp)
	{
		if ($this->getContainerAgent()->_enable_closure) {
			$this->setPropertyMapping($nameProp,
				function ($name) {
					if (array_key_exists($name, $this->db)) {
						return $this->db[$name];
					}
					return 'non';
				},
				function ($value, $name) {
					if (!is_null($value)) {
						$this->db[$name] = $value . '_postfix';
					} else {
						unset($this->db[$name]);
					}
				}
			);
		} else {
			$base = $this;
			$func1 = function ($name) use ($base) {
					$res = $base->getProp($name);
					return ($res == 'n/a') ? 'non' : $res;
				};
			$func2 = function ($value, $name) use ($base) {
					$base->setProp($value, $name);
				};
			$this->setPropertyMapping($nameProp, $func1, $func2);
		}
	}
	
	public function getPropArr($nameArr)
	{
		return print_r($this->$nameArr, true);
	}
}

?>
