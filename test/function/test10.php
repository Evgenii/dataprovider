<?php

include_once '../testClass.php';
include_once '../testClassTools.php';

$a = new A();
$t = new Tools_For_Container();

logHeader("set func: \$a->setFunctionMapping('getX1FromStore', \$t->getX1FromStore())");
logResFunc('getX1FromStore()', $a->getX1FromStore(), NULL);
$a->setFunctionMapping('getX1FromStore', array($t, 'getX1FromStore'));
logResFunc('getX1FromStore()', $a->getX1FromStore(), "variable 'x1' is not in 'store'");
$t->x1 = 'foo';
logRes('x1', $t, 'foo');
logResFunc('getX1FromStore()', $a->getX1FromStore(), 'foo', true);

logHeader("set func from other class: \$a->setFunctionMapping('getVarByName', \$t->getVarFromStore())");
$a->setFunctionMapping('getVarByName', array($t, 'getVarFromStore'));
logResFunc("getVarByName('x2')", $a->getVarByName('x2'), "var 'x2' is not in 'store'");
$t->x2 = 'bar';
logRes('x2', $t, 'bar');
logResFunc("getVarByName('x2')", $a->getVarByName('x2'), 'bar');
logResFunc("getVarByName('bla')", $a->getVarByName('bla'), "var 'bla' is not in 'store'", true);

logHeader("get func in \$cl: \$cl=\$a->getFunctionMapping('getVarByName')");
$cl = $a->getContainerAgent()->getFunctionMapping('getVarByName');
logResFunc("\$cl('x2')", $cl('x2'), 'bar');
logResFunc("\$cl('xx')", $cl('xx'), "var 'xx' is not in 'store'", true);

logHeader("delete func: \$a->setFunctionMapping('getVarByName', NULL)");
$a->setFunctionMapping('getVarByName', NULL);
logResFunc("getVarByName('x2')", $a->getVarByName('x2'), NULL);
logResFunc("\$cl('x2')", $cl('x2'), 'bar', true);

logHeader("set closure func: \$a->setFunctionMapping('getVarByName', \$cl)");
$a->setFunctionMapping('getVarByName', $cl);
logResFunc("getVarByName('x2')", $a->getVarByName('x2'), 'bar');
logResFunc("\$cl('x2')", $cl('x2'), 'bar', true);

logHeader("get func in \$cl2: \$cl2=\$a->getFunctionMapping('getVarByName')");
$cl2 = $a->getContainerAgent()->getFunctionMapping('getVarByName');
logResFunc("\$cl2('x2')", $cl2('x2'), 'bar', true);

logHeader("set with assign 'this': \$a->setFunctionMapping('getVarByName', \$cl, true)");
$a->setFunctionMapping('getVarByName', $cl, true);
logResFunc("getVarByName('x2')", $a->getVarByName('x2'), 'bar');
logResFunc("\$cl('x2')", $cl('x2'), 'bar', true);

logHeader("get closure from '\$t': \$cl3=\$t->getClosure()");
$cl3 = $t->getClosure($a->getContainerAgent()->_enable_closure);
logResFunc("\$cl3('x2')", $cl3('x2'), 'bar', true);

logHeader("set closure '\$cl3' without assign 'this': \$a->setFunctionMapping('getVarByName', \$cl3)");
$a->setFunctionMapping('getVarByName', $cl3);
logResFunc("getVarByName('x2')", $a->getVarByName('x2'), 'bar', true);

logHeader("set closure '\$cl3' with assign 'this': \$a->setFunctionMapping('getVarByName', \$cl3, true)");
$a->setFunctionMapping('getVarByName', $cl3, true);
logRemark("next command will be ERROR! because 'store' is not inside \$a");
logResFunc("getVarByName('x2')", $a->getVarByName('x2'), '~n/a~', true);

logHeader("get closure from '\$t': \$cl4=\$t->getClosureForContainer()");
$cl4 = $t->getClosureForContainer($a->getContainerAgent()->_enable_closure);
logRemark("next command will be ERROR! because '_container' is not inside \$t and PHP < 5.4.0");
logResFunc("\$cl4('x2')", $cl4('x2'), "var 'x2' is not in '_container'", true);

logHeader("set closure '\$cl4' with assign 'this': \$a->setFunctionMapping('getVarByName', \$cl4, true)");
$a->setFunctionMapping('getVarByName', $cl4, true);
$a->newVar = 'foo';
logRes('newVar', $a, 'foo');
logRemark("next command will be OK, because '_container' inside \$a, but ERROR for PHP < 5.4.0");
logResFunc("getVarByName('newVar')", $a->getVarByName('newVar'), 'foo', true);

?>
