<?php

class Tools_For_Aspect
{
	protected function getHeader($method)
	{
		$ts = microtime(true);
		return '[' . date("d.m.Y H:i:s", $ts) . " $ts] [$method]";
	}
	
// format: function($nameValue, $value) return $newValue
	public function preSetPropLog($name, $value)
	{
		$newValue = (is_string($value)) ? $value : "'$value' is not string";
		echo $this->getHeader('SET') . sprintf(" Pre-logging property '%s', get for writing '%s', return new '%s'", $name, $value, $newValue) . PHP_EOL;
		return $newValue;
	}
	
// format: function($nameValue, $value) return $newValue
	public function postGetPropLog($name, $value)
	{
// only not 'bar'!
		$newValue = ($value == 'bar') ? "old.$value" : $value;
		echo $this->getHeader('GET') . sprintf(" Post-logging property '%s', getting '%s', return new '%s'", $name, $value, $newValue) . PHP_EOL;
		return $newValue;
	}
	
// format: function($nameFunc, &$result, &$arguments) return true
	public function preSetPropLogFunc(spc\ProviderData\Negotiator $negotiator)
	{
		$oldResult = $negotiator->getResult();
		$arguments = &$negotiator->getArguments();
		$oldArguments = $negotiator->getOldArguments();
		if (is_numeric($arguments[0])) {
			$arguments[0] = 'xxx';
// or
//			$negotiator[0] = 'xxx';
		}
		echo $this->getHeader('SET') . sprintf(" Pre-logging function '%s', get arguments '%s', return new arguments '%s'", $negotiator->getName(), print_r($oldArguments, true), print_r($arguments, true)) . PHP_EOL;
		return true;
	}
	
// format: function($nameFunc, &$result, $arguments) return true
	public function postGetPropLogFunc(spc\ProviderData\Negotiator $negotiator)
	{
// only not 'foo'!
		$result = $negotiator->getResult();
		$oldResult = $result;
		if ($result == 'foo') {
			$result = "old.$result";
			$negotiator->setResult($result);
// or
//			$negotiator['result'] = $result;
		}
		echo $this->getHeader('GET') . sprintf(" Post-logging function '%s', getting '%s', return new '%s'", $negotiator->getName(), $oldResult, $result) . PHP_EOL;
		return true;
	}
}

class Tools_For_Container
{
	protected $store = array();
	
	public function __get($name)
	{
		if (array_key_exists($name, $this->store)) {
			return $this->store[$name];
		}
		return null;
	}
	
	public function __set($name, $value)
	{
		if (is_null($value)) {
			unset($this->store[$name]);
		} else {
			$this->store[$name] = $value;
		}
	}
	
	public function __isset($name)
	{
		return isset($this->store[$name]);
	}
	
	public function __unset($name)
	{
		unset($this->store[$name]);
	}
	
	public function getX1FromStore()
	{
		if (array_key_exists('x1', $this->store)) {
			return $this->store['x1'];
		}
		return "variable 'x1' is not in 'store'";
	}
	
	public function getVarFromStore($name = null)
	{
		if (array_key_exists($name, $this->store)) {
			return $this->store[$name];
		}
		return "var '$name' is not in 'store'";
	}
	
	public function getClosure($enable_closure = true)
	{
		if ($enable_closure) {
			return	function ($name) {
					if (array_key_exists($name, $this->store)) {
						return $this->store[$name];
					}
					return '~n/a~';
				};
		}
		$base = $this;
		return function ($name) use ($base) {
				$res = $base->getProp($name);
				return ($res == 'n/a') ? '~n/a~' : $res;
			};
	}
	
	public function getClosureForContainer($enable_closure = true)
	{
		if ($enable_closure) {
			return	function ($name) {
					if (array_key_exists($name, $this->_container)) {
						return $this->_container[$name];
					}
					return "var '$name' is not in '_container'";
				};
		}
		$base = $this;
		return function ($name) use ($base) {
				if (array_key_exists($name, $base->_container)) {
					return $base->_container[$name];
				}
				return "var '$name' is not in '_container'";
			};
	}
	
	public function setProp($value, $name)
	{
		if (!is_null($value)) {
			$this->store[$name] = 'a_' . $value;
		} else {
			unset($this->store[$name]);
		}
	}
	
	public function getProp($name)
	{
		if (array_key_exists($name, $this->store)) {
			return $this->store[$name];
		}
		return 'n/a';
	}
	
	public function setPropDatas($value, $name)
	{
		if (!is_null($value)) {
			$this->store[$name] = 'b_' . $value;
		} else {
			unset($this->store[$name]);
		}
	}
	
	public function getPropDatas($name)
	{
		if (array_key_exists($name, $this->store)) {
			return '[' . $this->store[$name] . ']';
		}
		return 'undefined';
	}
	
	public function setPropClosure($nameProp)
	{
		$this->setPropertyMapping($nameProp,
				function ($name) {
					if (array_key_exists($name, $this->store)) {
						return $this->store[$name];
					}
					return 'non';
				},
				function ($value, $name) {
					if (!is_null($value)) {
						$this->store[$name] = $value . '_closure';
					} else {
						unset($this->store[$name]);
					}
				}
			);
	}
	
	public function getPropArr($nameArr)
	{
		return print_r($this->$nameArr, true);
	}
}

?>
