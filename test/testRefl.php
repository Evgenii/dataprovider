<?php

//include_once 'testClass.php';

class B
{
	public $db = array();
	public $varNull = null;
	public $varStr = 'Sample text.';
	public $varUndef;
	public static $varStatic;
	protected $varProt = 'Protected text.';
	private $varPrivate = 'Private text.';
}

$a = new B();

$r1 = new ReflectionClass($a);
var_dump($r1->getDefaultProperties());

var_dump(isset($a->varStr));	// true
$a->varStr = null;
var_dump(isset($a->varStr));	// false
echo PHP_EOL;

var_dump(isset($a->varNull));	// false
$a->varNull = 'this_is_null';
var_dump(isset($a->varNull));	// true
unset($a->varNull);
var_dump(isset($a->varNull));	// false
echo PHP_EOL;

$a->bla = 'sample1';
echo 'bla=' . $a->bla . PHP_EOL;	// bla=sample1
echo PHP_EOL;

$r2 = new ReflectionClass($a);
var_dump($r2->getProperties());
var_dump($r2->hasProperty('varPrivate'));	// true
var_dump($r2->hasProperty('varStr'));		// true
var_dump($r2->hasProperty('varNull'));		// true
var_dump($r2->hasProperty('varStatic'));	// true
var_dump($r2->hasProperty('bla'));		// false

echo PHP_EOL . ' *** 1 ***' . PHP_EOL;

$r2->getProperty('varUndef')->setValue($a, 'writeToUndef');
try {
	$r2->getProperty('newVar')->setValue($a, 'writeToNew');
} catch (ReflectionException $e) {
	echo 'Error: ' . $e->getMessage() . PHP_EOL;
}

try {
	$prop = $r2->getProperty('varProt');
	$prop->setAccessible(true);
	var_dump($prop->isProtected());	// true
	var_dump($prop->isPrivate());	// false
	var_dump($prop->isPublic());	// false
	echo '$a->varProt = ' . $prop->getValue($a) . PHP_EOL;
	$prop->setValue($a, 'writeToProt');
	echo '$a->varProt = ' . $prop->getValue($a) . PHP_EOL;
} catch (ReflectionException $e) {
	echo 'Error: ' . $e->getMessage() . PHP_EOL;
}

echo PHP_EOL . ' *** 2 ***' . PHP_EOL;

var_dump(isset($a->varUndef));		// true
var_dump(isset($a->varProt));		// false
var_dump(isset($a->varPrivate));	// false
var_dump(isset($a->varStatic));		// false

echo PHP_EOL . ' *** 3 ***' . PHP_EOL;

try {
	$prop = $r2->getProperty('varStatic');
	var_dump($prop->isProtected());	// false
	var_dump($prop->isPrivate());	// false
	var_dump($prop->isPublic());	// true
	var_dump($prop->isStatic());	// true
	echo '$a->varStatic = ' . $prop->getValue($a) . PHP_EOL;
	$prop->setValue($a, 'writeToStatic');
	echo '$a->varStatic = ' . $prop->getValue($a) . PHP_EOL;
} catch (ReflectionException $e) {
	echo 'Error: ' . $e->getMessage() . PHP_EOL;
}
?>
