<?php

use \spc\ProviderData as pd;

include_once '../testClassSetterArray.php';

$data = array('x1' => 'foo', 1 => 'test1', 4 => 'test2', 3 => 'test3', 'x2' => 'bar');

$a = new B($data);

logHeader("foreach (\$a as \$key => \$val)");
foreach ($a as $key => $val) {
	logRemark("\$a['$key']=$val");
}
echo PHP_EOL;

//logHeader("\$a->x1 or \$a['x1']");
//logRes('x1', $a, 'newValue');
//logResFunc("\$a['x1']", $a['x1'], 'newValue', true);

?>
