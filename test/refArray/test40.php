<?php

use \spc\ProviderData as pd;

include_once '../testClassSetterArray.php';
include_once '../testClassTools.php';

$data = array('x1' => 'foo', 1 => 'test1', 4 => 'test2', 3 => 'test3', 'x2' => 'bar');

$a = new B($data);
$c = new Tools_For_Aspect();

logHeader("foreach (\$a as \$key => \$val)");
foreach ($a as $key => $val) {
	logRemark("\$a['$key']=$val");
}
echo PHP_EOL;

logHeader("\$a['x1'] = 'newValue'");
$a['x1'] = 'newValue';
logRes('x1', $a, 'newValue', true);

//logHeader("\$a->offsetContainer(\$data)", true);
//$a->offsetContainer($data);

logHeader("\$a->x1 or \$a['x1']");
logRes('x1', $a, 'newValue');
logResFunc("\$a['x1']", $a['x1'], 'newValue', true);

logHeader("\$data['x1'] = 'blabla'", true);
$data['x1'] = 'blabla';

logHeader("foreach (\$a as \$key => \$val)");
foreach ($a as $key => $val) {
	logRemark("\$a['$key']=$val");
}
echo PHP_EOL;

logHeader("setPropertyMappingGet('x1', 'getPropDatas')", true);
$a->setPropertyMappingGet('x1', 'getPropDatas');

logHeader("\$a->x1 or \$a['x1']");
logRes('x1', $a, '[blabla]');
logResFunc("\$a['x1']", $a['x1'], '[blabla]', true);

logHeader("foreach (\$a as \$key => \$val)");
foreach ($a as $key => $val) {
	logRemark("\$a['$key']=$val");
}
echo PHP_EOL;

logHeader("setFunctionAspect(_TRIGGER_POST, 'x1', \$c->postGetPropLog())", true);
$a->setFunctionAspect(pd\Agent::_TRIGGER_POST, 'x1', array($c, 'postGetPropLog'));

logHeader("setFunctionAspect(_TRIGGER_POST, NULL, \$c->postGetPropLog())", true);
$a->setFunctionAspect(pd\Agent::_TRIGGER_POST, NULL, array($c, 'postGetPropLog'));

logHeader("foreach (\$a as \$key => \$val)");
foreach ($a as $key => $val) {
	logRemark("\$a['$key']=$val");
}
echo PHP_EOL;

?>
