<?php

//include_once 'testClass.php';

class A
{
	public function test()
	{
		return 'A->test';
	}
	
	public static function testStatic()
	{
		return 'A::testStatic';
	}
}

class B
{
	public function test()
	{
		return 'B->test';
	}
	
	public function __invoke()
	{
		return 'B->__invoke';
	}
}

function testN ()
{
	return 'testN';
}

function testCallable (callable $call)
{
	return 'callable';
}

function testCall ($n, $call)
{
	try {
		if (is_callable($call)) {
			echo "testCall $n || " . call_user_func($call) . ' || OK' . PHP_EOL;
		} else {
			$obj = (is_array($call)) ? $call[0] : $call;
			echo "testCall $n || " . get_class($obj) . ' || No' . PHP_EOL;
		}
	} catch (Exception $e) {
		echo 'Error: ' . $e->getMessage() . PHP_EOL;
	}
	
	try {
//		echo "testCallable $n || " . testCallable($call) . ' || OK' . PHP_EOL;
	} catch (Exception $e) {
		echo 'Error: ' . $e->getMessage() . PHP_EOL;
	}
}

$func = function () {
		return 'Closure';
	};

$a = new A();
$b = new B();

$ra = new ReflectionClass($a);
$rb = new ReflectionClass($b);

$raTest = $ra->getMethod('test');
$raTestStatic = $ra->getMethod('testStatic');
$rbInvoke = $rb->getMethod('__invoke');

testCall(0, 'testN');
testCall(1, array('A', 'testStatic'));
testCall(2, array($a, 'test'));
testCall(3, array($a, 'testStatic'));
testCall(4, $a);
testCall(5, $b);
testCall(6, $func);
testCall(7, $ra);
testCall(8, $rb);
testCall(9, $raTest);
testCall(10, $raTestStatic);
testCall(11, $rbInvoke);

echo $raTestStatic->invoke(null) . PHP_EOL;

?>
