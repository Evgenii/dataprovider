<?php

include_once '../testClass.php';

$a = new A();

logHeader("self public variable");
logRes('var1', $a, 'foo');
$a->var1 = 'unset_var1';
$a->var1 = null;
logRes('var1', $a, NULL);
$a->var1 = 'propertySet_change';
logRes('var1', $a, 'propertySet_change', true);

logHeader("create new public variable");
$a->var2 = 'bla';
logRes('var2', $a, 'bla');
$a->var2 = null;
logRes('var2', $a, NULL, true);

logHeader("create new public second variable");
$a->var3 = 'opa3';
logRes('var3', $a, 'opa3');
$a->var3 = null;
logRes('var3', $a, NULL);
$a->var3 = 'common';
logRes('var3', $a, 'common', true);

logHeader("verify read undefined variable");
logRes('var4', $a, NULL, true);

logHeader("self public variable with setter/getter");
logRes('varSetter', $a, 'setter');
$a->varSetter = 'change';
logRes('varSetter', $a, 'change');
$a->varSetter = null;
logRes('varSetter', $a, NULL);
$a->varSetter = 'propertySet_change';
logRes('varSetter', $a, 'propertySet_change');
$a->varSetter = null;
logRes('varSetter', $a, NULL);
?>
