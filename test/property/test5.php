<?php

include_once '../testClassSetter.php';

$a = new B();

logHeader("setPropertyMapping('x1', 'getPropDatas', 'setPropDatas')");
logRes('x1', $a, NULL);
$a->setPropertyMapping('x1', 'getPropDatas', 'setPropDatas');
logRes('x1', $a, 'non in _datas');
logResDump('x1', var_export(isset($a->x1), true), '1(true)');
$a->x1 = 'super';
logRes('x1', $a, '[pre_super]');
logResDump('x1', var_export(isset($a->x1), true), '1(true)');
unset($a->x1);
logRes('x1', $a, NULL);
logResDump('x1', var_export(isset($a->x1), true), '0(false)', true);

logHeader("setPropertyMapping('x2', NULL, 'setPropDatas')");
logRes('x2', $a, NULL);
$a->setPropertyMapping('x2', NULL, 'setPropDatas');
logRes('x2', $a, NULL);
logResDump('x2', var_export(isset($a->x2), true), '1(true)');
$a->x2 = 'cool';
logRes('x2', $a, 'pre_cool');
logResDump('x2', var_export(isset($a->x2), true), '1(true)');
unset($a->x2);
logRes('x2', $a, NULL);
logResDump('x2', var_export(isset($a->x2), true), '0(false)', true);

logHeader("setPropertyMapping('x3', 'getPropDatas', NULL)");
logRes('x3', $a, NULL);
$a->setPropertyMapping('x3', 'getPropDatas', NULL);
logRes('x3', $a, 'non in _datas');
logResDump('x3', var_export(isset($a->x3), true), '1(true)');
$a->x3 = 'good';
logRes('x3', $a, '[good]');
logResDump('x3', var_export(isset($a->x3), true), '1(true)');
unset($a->x3);
logRes('x3', $a, NULL);
logResDump('x3', var_export(isset($a->x3), true), '0(false)', true);

?>
