<?php

include_once '../testClass.php';

$a = new A();

logHeader("unset self public variable");
logRes('var1', $a, 'foo');
$a->var1 = 'write_to_var1';
logRes('var1', $a, 'write_to_var1');
unset($a->var1);
logRes('var1', $a, NULL);
$a->var1 = 'propertySet_change';
logRes('var1', $a, 'propertySet_change', true);

logHeader("create new public variable and unset");
$a->var2 = 'bla';
logRes('var2', $a, 'bla');
unset($a->var2);
logRes('var2', $a, NULL, true);

logHeader("verify unset undefined variable");
unset($a->var3);
logRes('var3', $a, NULL, true);

logHeader("isset self public variable");
logResDump('var1', var_export(isset($a->var1), true), '1(true)');
unset($a->var1);
logResDump('var1', var_export(isset($a->var1), true), '1(true)');
logRes('var1', $a, NULL, true);

logHeader("isset new public variable");
logResDump('varnew', var_export(isset($a->varnew), true), '0(false)');
$a->varnew = 'foobar';
logResDump('varnew', var_export(isset($a->varnew), true), '1(true)');
logRes('varnew', $a, 'foobar');
unset($a->varnew);
logResDump('varnew', var_export(isset($a->varnew), true), '0(false)');
logRes('varnew', $a, NULL, true);

// todo with unset($a->x) and isset($a->x) with mapping properties

?>
