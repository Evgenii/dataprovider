<?php

include_once '../testClassSetter.php';

class C
{
	protected $a;
	protected $db = array();
	
	public function __construct($a)
	{
		$this->a = $a;
	}
	
	public function getProp($name)
	{
		if (isset($this->a->$name)) {
			return '~' . $this->a->test3($name, 'from C') . '~';
		}
		return '---';
	}
}

$a = new B();
$c = new C($a);

logRes('x1', $a, null);
$a->x1 = 'test1';
logRes('x1', $a, 'test1', true);

logHeader("setPropertyMapping('x1', \$c->getProp, 'setProp')");
$a->setPropertyMapping('x1', array($c, 'getProp'), 'setProp');
logRes('x1', $a, '~test1|from C|~', true);

logHeader("\$a->x1 = 'test2' ! different store");
$a->x1 = 'test2';
logRes('x1', $a, '~test1|from C|~', true);

logHeader("setPropertyMapping('x1', NULL, NULL)");
$a->setPropertyMapping('x1', NULL, NULL);
logRes('x1', $a, 'test1', true);

logHeader("setPropertyMapping('x1', \$c->getProp, 'setPropDatas')");
$a->setPropertyMapping('x1', array($c, 'getProp'), 'setPropDatas');
logRes('x1', $a, '~test1|from C|~', true);

logHeader("\$a->x1 = 'test3' ! equal store");
$a->x1 = 'test3';
logRes('x1', $a, '~pre_test3|from C|~', true);

logHeader("setPropertyMapping('x1', NULL, 'setProp')");
$a->setPropertyMapping('x1', NULL, 'setProp');
logRes('x1', $a, 'pre_test3', true);

logHeader("\$a->x1 = 'test4'  ! different store");
$a->x1 = 'test4';
logRes('x1', $a, 'pre_test3', true);

logResDump('_datas', $a->getPropArr('_datas'), '[x1] => pre_test3', true);

logResDump('db', $a->getPropArr('db'), '[x1] => prefix_test4', true);

logHeader("setPropertyMapping('x2', 'getPropDatas', NULL)");
$a->setPropertyMapping('x2', 'getPropDatas', NULL);
logRes('x2', $a, 'non in _datas', true);

logHeader("\$a->x2 = 'yes'");
$a->x2 = 'yes';
logRes('x2', $a, '[yes]', true);

?>
