<?php

include_once '../testClassSetter.php';

class C
{
	protected $db = array();
	
	public function setProp($value, $name)
	{
		$this->db[$name] = '~' . $value . '~';
	}
	
	public function getProp($name)
	{
		if (array_key_exists($name, $this->db)) {
			return $this->db[$name];
		}
		return '---';
	}
}

$a = new B();
$c = new C();

logHeader("setPropertyMapping(<new>, <string> , <string>)");
logRes('dbext', $a, NULL);
$a->setPropertyMapping('dbext', 'getProp', 'setProp');
logRes('dbext', $a, 'n/a');
$a->dbext = 'sample1';
logRes('dbext', $a, 'prefix_sample1');
// ---
$a->setPropertyMapping('dbint', 'getProp', 'setProp');
$a->dbint = 'sample2';
logRes('dbint', $a, 'prefix_sample2', true);

logHeader("setPropertyMapping(<new>, <closure> , <closure>)");
logRes('fileext', $a, NULL);
$a->setPropClosure('fileext');
logRes('fileext', $a, 'non');
$a->fileext = 'text1';
logRemark("next TEST for PHP < 5.4.0, output 'prefix_text1'");
logRes('fileext', $a, 'text1_postfix', true);

logHeader("setPropertyMapping(<new>, <other class> , <other class>)");
logRes('resext', $a, NULL);
$a->setPropertyMapping('resext', array($c, 'getProp'), array($c, 'setProp'));
logRes('resext', $a, '---');
$a->resext = 'text4C';
logRes('resext', $a, '~text4C~', true);

logHeader("setPropertyMapping(<self>, <string> , <string>)");
logRes('var1', $a, 'foo');
logResDump("var_export(\$a->setPropertyMapping('var1', 'getProp', 'setProp'))", var_export($a->setPropertyMapping('var1', 'getProp', 'setProp'), true), 'false');
logRes('var1', $a, 'foo');
$a->var1 = 'sample1';
logRes('var1', $a, 'sample1', true);

//
logHeader("setPropertyMapping(<from container>, <string> , <string>)");
logRemark('ss put in $_datas');
$a->ss = 'in container';
logRes('ss', $a, 'in container');

logRemark("setPropertyMapping('ss', 'getProp', 'setProp')");
$a->setPropertyMapping('ss', 'getProp', 'setProp');
logRes('ss', $a, 'n/a');

logRemark("setPropertyMapping('ss', 'getPropDatas', 'setPropDatas')");
$a->setPropertyMapping('ss', 'getPropDatas', 'setPropDatas');
logRes('ss', $a, '[in container]');

logRemark("\$a->ss = 'in container'");
$a->ss = 'in container';
logRes('ss', $a, '[pre_in container]');

logRemark("setPropertyMapping('ss', 'getProp', 'setProp')");
$a->setPropertyMapping('ss', 'getProp', 'setProp');
logRes('ss', $a, 'n/a');

logRemark("\$a->ss = 'in other container'");
$a->ss = 'in other container';
logRes('ss', $a, 'prefix_in other container');

logRemark("setPropertyMapping('ss', NULL, NULL)");
$a->setPropertyMapping('ss', null, null);
logRes('ss', $a, 'pre_in container');

//$a->ss = null;

logRemark("setPropertyMapping('ss', 'getPropDatas', 'setPropDatas')");
$a->setPropertyMapping('ss', 'getPropDatas', 'setPropDatas');
logRes('ss', $a, '[pre_in container]');

logRemark("\$a->ss = NULL");
$a->ss = null;
logRes('ss', $a, 'non in _datas', true);

logRemark("\$a->ss = 'new container'");
$a->ss = 'new container';
logRes('ss', $a, '[pre_new container]');

logRemark("setPropertyMapping('ss', 'getPropDatas', 'setPropDatas')");
$a->setPropertyMapping('ss', 'getPropDatas', 'setPropDatas');
logRes('ss', $a, '[pre_new container]');

logRemark("unset(\$a->ss)");
unset($a->ss);
logRes('ss', $a, NULL, true);

logResDump('_datas', $a->getPropArr('_datas'), '', true);

logRemark("next TEST for PHP < 5.4.0, output '[fileext] => prefix_text1'");
logResDump('db', $a->getPropArr('db'), "[dbext] => prefix_sample1
    [dbint] => prefix_sample2
    [fileext] => text1_postfix
    [ss] => prefix_in other container", true);

logRemark("\$c->bla = 'sample1'");
$c->bla = 'sample1';
logRes('bla', $c, 'sample1', true);

?>
