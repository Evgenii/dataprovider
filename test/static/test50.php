<?php

use \spc\ProviderData as pd;

include_once '../testClassStatic.php';
include_once '../testClassTools.php';

$c = new Tools_For_Aspect();
$t = new Tools_For_Container();

B::sampleFunction('test1');

logHeader("set func: B::setFunctionMapping('getX1FromStore', \$t->getX1FromStore())");
logResFunc('B::getX1FromStore()', B::getX1FromStore(), NULL);
B::setFunctionMapping('getX1FromStore', array($t, 'getX1FromStore'));
logResFunc('B::getX1FromStore()', B::getX1FromStore(), "variable 'x1' is not in 'store'");
$t->x1 = 'foo';
logRes('x1', $t, 'foo');
logResFunc('B::getX1FromStore()', B::getX1FromStore(), 'foo', true);

logHeader("set func: B::setFunctionMapping('getVarFromStore', \$t->getVarFromStore())");
B::setFunctionMapping('getVarFromStore', array($t, 'getVarFromStore'));
logResFunc("B::getVarFromStore('xx')", B::getVarFromStore('xx'), "var 'xx' is not in 'store'");
logResFunc("B::getVarFromStore('x1')", B::getVarFromStore('x1'), "foo", true);

logHeader("B::setFunctionAspect(_FUNC_POST, 'getVarFromStore', \$c->postGetPropLogFunc())");
B::setFunctionAspect(pd\Agent::_FUNC_POST, 'getVarFromStore', array($c, 'postGetPropLogFunc'));
logRemark("B::setFunctionAspect(_FUNC_PRE, 'getVarFromStore', \$c->preSetPropLogFunc())", true);
B::setFunctionAspect(pd\Agent::_FUNC_PRE, 'getVarFromStore', array($c, 'preSetPropLogFunc'));

logHeader("run func: B::getVarFromStore('?')");
logResFunc("B::getVarFromStore('xx')", B::getVarFromStore('xx'), "var 'xx' is not in 'store'");
logResFunc("B::getVarFromStore('x1')", B::getVarFromStore('x1'), "old.foo", true);

logHeader("set func: B::setFunctionMapping('testFunc', 'getProp')");
logResFunc('B::testFunc()', B::testFunc('x1'), NULL);
B::setFunctionMapping('testFunc', 'getProp');
logResFunc('B::testFunc()', B::testFunc('x1'), 'n/a');

?>
