<?php

include_once 'testClass.php';

class B extends A
{
	static $_db = array();
	
	public static function setProp($value, $name)
	{
		if (!is_null($value)) {
			self::$_db[$name] = 'prefix_' . $value;
		} else {
			unset(self::$_db[$name]);
		}
	}
	
	public static function getProp($name)
	{
		if (array_key_exists($name, self::$_db)) {
			return self::$_db[$name];
		}
		return 'n/a';
	}
	
// todo
	public function setPropClosure($nameProp)
	{
		if ($this->getContainerAgent()->_enable_closure) {
			$this->setPropertyMapping($nameProp,
				function ($name) {
					if (array_key_exists($name, $this->db)) {
						return $this->db[$name];
					}
					return 'non';
				},
				function ($value, $name) {
					if (!is_null($value)) {
						$this->db[$name] = $value . '_postfix';
					} else {
						unset($this->db[$name]);
					}
				}
			);
		} else {
			$base = $this;
			$func1 = function ($name) use ($base) {
					$res = $base->getProp($name);
					return ($res == 'n/a') ? 'non' : $res;
				};
			$func2 = function ($value, $name) use ($base) {
					$base->setProp($value, $name);
				};
			$this->setPropertyMapping($nameProp, $func1, $func2);
		}
	}
}

?>
