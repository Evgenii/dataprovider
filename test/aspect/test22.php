<?php

use \spc\ProviderData as pd;

include_once '../testClassSetterArrayBB.php';
include_once '../testClassTools.php';

$a = new BB();
$c = new Tools_For_Aspect();
$t = new Tools_For_Container();

logHeader("set func: \$a->setFunctionMapping('getVarFromStore', \$t->getVarFromStore())");
logResFunc("getVarFromStore('x1')", $a->getVarFromStore('x1'), NULL);
$a->setFunctionMapping('getVarFromStore', array($t, 'getVarFromStore'));
logResFunc("getVarFromStore('x1')", $a->getVarFromStore('x1'), "var 'x1' is not in 'store'");
logRemark("\$t->x1='foo'");
$t->x1 = 'foo';
logRes('x1', $t, 'foo');
logResFunc("getVarFromStore('x1')", $a->getVarFromStore('x1'), 'foo', true);

logHeader("setFunctionAspect(_FUNC_POST, 'getVarFromStore', \$c->postGetPropLogFunc())");
$a->setFunctionAspect(pd\Agent::_FUNC_POST, 'getVarFromStore', array($c, 'postGetPropLogFunc'));
logRemark("setFunctionAspect(_FUNC_PRE, 'getVarFromStore', \$c->preSetPropLogFunc())", true);
$a->setFunctionAspect(pd\Agent::_FUNC_PRE, 'getVarFromStore', array($c, 'preSetPropLogFunc'));

logHeader("getVarFromStore('x1')");
logResFunc("getVarFromStore('x1')", $a->getVarFromStore('x1'), 'old.foo', true);

logHeader("getVarFromStore(1223)");
logResFunc("getVarFromStore(1223)", $a->getVarFromStore(1223), "var 'xxx' is not in 'store'", true);

$cl = $a->getClosure($t);
logHeader("setFunctionAspect(_FUNC_AROUND, 'getVarFromStore', \$a->getClosure(\$t))", true);
$a->setFunctionAspect(pd\Agent::_FUNC_AROUND, 'getVarFromStore', $cl);

logHeader("getVarFromStore('x1')");
logResFunc("getVarFromStore('x1')", $a->getVarFromStore('x1'), '~n/a~', true);

logRemark("\$a->x1='bar'");
$a->x1 = 'bar';
logRes('x1', $a, 'bar', true);

logHeader("getVarFromStore('x1')");
logResFunc("getVarFromStore('x1')", $a->getVarFromStore('x1'), 'bar', true);

logHeader("getVarFromStore(1223)");
logResFunc("getVarFromStore(1223)", $a->getVarFromStore(1223), '~n/a~', true);

logHeader("setFunctionAspect(_FUNC_PRE, 'getVarFromStore', NULL)", true);
$a->setFunctionAspect(pd\Agent::_FUNC_PRE, 'getVarFromStore', NULL);

logHeader("getVarFromStore(1223)");
logResFunc("getVarFromStore(1223)", $a->getVarFromStore(1223), "var '1223' is not in 'store'", true);

?>
