<?php

use \spc\ProviderData as pd;

include_once '../testClassSetterArray.php';
include_once '../testClassTools.php';

$a = new B();
$c = new Tools_For_Aspect();

logHeader("\$a->x1");
logRes('x1', $a, NULL);
logResDump("isset(\$a->x1)", var_export(isset($a->x1), true), '0(false)', true);

logHeader("\$a->x1='foo'");
$a->x1 = 'foo';
logRes('x1', $a, 'foo');
logResDump("isset(\$a->x1)", var_export(isset($a->x1), true), '1(true)', true);

logHeader("setFunctionAspect(_TRIGGER_PRE, 'x1', \$c->preSetPropLog())");
$a->setFunctionAspect(pd\Agent::_TRIGGER_PRE, 'x1', array($c, 'preSetPropLog'));
logRemark("setFunctionAspect(_TRIGGER_POST, 'x1', \$c->postGetPropLog())");
$a->setFunctionAspect(pd\Agent::_TRIGGER_POST, 'x1', array($c, 'postGetPropLog'));
logRes('x1', $a, 'foo');
logRemark("\$a->x1='bar'");
$a->x1 = 'bar';
logResFunc('x1', $a['x1'], 'old.bar');
logRemark("\$a->x1=231");
$a->x1 = 231;
logRes('x1', $a, "'231' is not string");

?>
