<?php

use \spc\ProviderData as pd;

include_once '../testClassSetterArray.php';
include_once '../testClassTools.php';

$a = new B();
$c = new Tools_For_Aspect();

$num = 1;

logHeader("\$a->1 or \$a[1]");
logRes('1', $a, NULL);
logResFunc("\$a[1]", $a[1], NULL);
logResDump("isset(\$a->1)", var_export(isset($a->$num), true), '0(false)');
logResDump("isset(\$a[1])", var_export(isset($a[1]), true), '0(false)', true);

logHeader("\$a[] = 'foo'");
$a[] = 'foo';
logRes('0', $a, 'foo');
logResFunc("\$a[0]", $a[0], 'foo', true);

logHeader("\$a[] = 'bar'");
$a[] = 'bar';
$num = 1;
logRes('1', $a, 'bar');
logResFunc("\$a[1]", $a[1], 'bar');
logResDump("isset(\$a->1)", var_export(isset($a->$num), true), '1(true)');
logResDump("isset(\$a[1])", var_export(isset($a[1]), true), '1(true)', true);

logHeader("setFunctionAspect(_TRIGGER_PRE, NULL, \$c->preSetPropLog())");
$a->setFunctionAspect(pd\Agent::_TRIGGER_PRE, NULL, array($c, 'preSetPropLog'));
logRemark("setFunctionAspect(_TRIGGER_POST, NULL, \$c->postGetPropLog())");
$a->setFunctionAspect(pd\Agent::_TRIGGER_POST, NULL, array($c, 'postGetPropLog'));
logResFunc("\$a[1]", $a[1], 'old.bar', true);

logHeader("\$a[33]=1423");
$a[33] = 1423;
logResFunc("\$a[33]", $a[33], "'1423' is not string", true);

logHeader("\$a->getPropArr('_datas')");
logResDump('_datas', $a->getPropArr('_datas'), "[0] => foo
    [1] => bar
    [33] => '1423' is not string", true);

logHeader("foreach (\$a as \$key => \$val)");
logResFunc("count(\$a)", count($a), 3);
foreach ($a as $key => $val) {
	logRemark("\$a[$key]=$val");
}
echo PHP_EOL;

logHeader("setFunctionAspect(_TRIGGER_POST, NULL, NULL)");
$a->setFunctionAspect(pd\Agent::_TRIGGER_POST, NULL, NULL);
logRemark("foreach (\$a as \$key => \$val)");
logResFunc("count(\$a)", count($a), 3);
foreach ($a as $key => $val) {
	logRemark("\$a[$key]=$val");
}

?>
