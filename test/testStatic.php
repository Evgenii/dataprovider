<?php

//include_once 'testClass.php';

class B
{
	public $varUndef;
	public static $varStatic;
	protected $varProt = 'Protected text.';
	private $varPrivate = 'Private text.';
	
	public function __get($name)
	{
		echo " === __get($name)" . PHP_EOL;
	}
	
	public function __set($name, $value)
	{
		echo " === __set($name), '$value'" . PHP_EOL;
	}
	
	public function __call($name, $arguments)
	{
		echo " === __call($name), " . print_r($arguments, true) . PHP_EOL;
	}
	
	public static function __callStatic($name, $arguments)
	{
		echo " === __callStatic($name), " . print_r($arguments, true) . PHP_EOL;
	}
	
	public function pub($str)
	{
		echo " === \$a->pub($str)" . PHP_EOL;
	}
	
	public static function pubStatic($str)
	{
		echo " === B::pubStatic($str)" . PHP_EOL;
	}
	
	protected function prot($str)
	{
		echo " === \$a->prot($str)" . PHP_EOL;
	}
}

$a = new B();

$r2 = new ReflectionClass($a);
var_dump($r2->getProperties());

echo PHP_EOL . ' *** 1 ***' . PHP_EOL;
try {
	B::$varStatic = 'test1';
	echo 'B::varStatic = ' . $r2->getProperty('varStatic')->getValue($a) . PHP_EOL;
//	B::$varStat = 'test2';
} catch (Exception $e) {
	echo 'Error: ' . $e->getMessage() . PHP_EOL;
}


echo PHP_EOL . ' *** 2 ***' . PHP_EOL;
try {
	echo 'echo B::varStatic = ' . B::$varStatic . PHP_EOL;
//	echo 'echo B::varStat = ' . B::$varStat . PHP_EOL;
} catch (Exception $e) {
	echo 'Error: ' . $e->getMessage() . PHP_EOL;
}

echo PHP_EOL . ' *** 3 ***' . PHP_EOL;
$a->myFunc1('test3');
B::myFunc2('test4');

echo PHP_EOL . ' *** 4 ***' . PHP_EOL;
try {
	$r2->setStaticPropertyValue('varStatic', 'test5');
	echo 'echo B::varStatic = ' . B::$varStatic . PHP_EOL;
	$r2->setStaticPropertyValue('varStat', 'test6');
} catch (ReflectionException $e) {
	echo 'Error: ' . $e->getMessage() . PHP_EOL;
}
var_dump($r2->hasProperty('varStat'));	// false

echo PHP_EOL . ' *** 5 ***' . PHP_EOL;
try {
	$r2->getMethod('pub')->invoke($a, 'test7');
	$r2->getMethod('pubStatic')->invoke('B', 'test8');
	$refl = $r2->getMethod('prot');
	$refl->setAccessible(true);
	$refl->invoke($a, 'test9');
	$r2->getMethod('prot')->invoke($a, 'test10');
} catch (ReflectionException $e) {
	echo 'Error: ' . $e->getMessage() . PHP_EOL;
}

?>
