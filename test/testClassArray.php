<?php

include_once '../../ContainerArray.php';
include_once 'tools.php';

class A extends \spc\ProviderData\ContainerArray
{
	public $var1 = 'foo';
	public $varSetter = 'setter';
	
	public function test1($s = '')
	{
		return $this->var1 . "|$s|";
	}
	
	public function test2($var, $s = '')
	{
		return $this->$var . "|$s|";
	}
	
	public function test3($var, $s = '')
	{
		return $this->_container[$var] . "|$s|";
	}
	
	public function getVarSetter()
	{
		return $this->varSetter;
	}
	
	public function setVarSetter($value)
	{
		$this->varSetter = $value;
	}
}
