<?php

include_once '../testClassSetterArray.php';

class C
{
	protected $db = array();
	
	public function setProp($value, $name)
	{
		$this->db[$name] = '~' . $value . '~';
	}
	
	public function getProp($name)
	{
		if (array_key_exists($name, $this->db)) {
			return $this->db[$name];
		}
		return '---';
	}
}

$a = new B();
$c = new C();

$num = 1;

logHeader("\$a->1 or \$a[1]");
logRes('1', $a, NULL);
logResFunc("\$a[1]", $a[1], NULL);
logResDump("isset(\$a->1)", var_export(isset($a->$num), true), '0(false)');
logResDump("isset(\$a[1])", var_export(isset($a[1]), true), '0(false)', true);

logHeader("\$a[] = 'foo'");
$a[] = 'foo';
logRes('0', $a, 'foo');
logResFunc("\$a[0]", $a[0], 'foo', true);

logHeader("\$a[] = 'bar'");
$a[] = 'bar';
$num = 1;
logRes('1', $a, 'bar');
logResFunc("\$a[1]", $a[1], 'bar');
logResDump("isset(\$a->1)", var_export(isset($a->$num), true), '1(true)');
logResDump("isset(\$a[1])", var_export(isset($a[1]), true), '1(true)', true);

logHeader("\$a[] = 'good'");
$a[] = 'good';
$num = 2;
logRes('2', $a, 'good');
logResFunc("\$a[2]", $a[2], 'good');
logResDump("isset(\$a->2)", var_export(isset($a->$num), true), '1(true)');
logResDump("isset(\$a[2])", var_export(isset($a[2]), true), '1(true)', true);

logHeader("\$a[] = 'super'");
$a[] = 'super';
$num = 3;
logRes('3', $a, 'super');
logResFunc("\$a[3]", $a[3], 'super');
logResDump("isset(\$a->3)", var_export(isset($a->$num), true), '1(true)');
logResDump("isset(\$a[3])", var_export(isset($a[3]), true), '1(true)', true);

logHeader("\$a[1] = 'ok'");
$a[1] = 'ok';
$num = 1;
logRes('1', $a, 'ok');
logResFunc("\$a[1]", $a[1], 'ok');
logResDump("isset(\$a->1)", var_export(isset($a->$num), true), '1(true)');
logResDump("isset(\$a[1])", var_export(isset($a[1]), true), '1(true)', true);

logHeader("\$a[2] = NULL");
$num = 2;
$a[$num] = null;
logRes('2', $a, NULL);
logResFunc("\$a[2]", $a[2], NULL);
logResDump("isset(\$a->2)", var_export(isset($a->$num), true), '0(false)');
logResDump("isset(\$a[2])", var_export(isset($a[2]), true), '0(false)');
logResDump('_datas', $a->getPropArr('_datas'), '[0] => foo
    [1] => ok
    [2] => 
    [3] => super', true);

logHeader("unset(\$a[1])");
unset($a[1]);
$num = 1;
logRes('1', $a, NULL);
logResFunc("\$a[1]", $a[1], NULL);
logResDump("isset(\$a->1)", var_export(isset($a->$num), true), '0(false)');
logResDump("isset(\$a[1])", var_export(isset($a[1]), true), '0(false)', true);

logHeader("\$a->x1 = 'blabla'");
$a->x1 = 'blabla';
logRes('x1', $a, 'blabla', true);

logHeader("setPropertyMapping('x2', \$c->getProp, \$c->setProp)");
$a->setPropertyMapping('x2', array($c, 'getProp'), array($c, 'setProp'));
$a->x2 = 'sample';
logRes('x2', $a, '~sample~', true);

logHeader("\$a->getPropArr('_datas')");
logResDump('_datas', $a->getPropArr('_datas'), '[0] => foo
    [2] => 
    [3] => super
    [x1] => blabla', true);

logHeader("foreach (\$a as \$key => \$val)");
logResFunc("count(\$a)", count($a), 5);
foreach ($a as $key => $val) {
	logRemark("\$a[$key]=$val");
}

?>
