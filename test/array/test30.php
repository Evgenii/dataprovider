<?php

include_once '../testClassSetterArray.php';

class C
{
	protected $db = array();
	
	public function setProp($name, $value)
	{
		$this->db[$name] = '~' . $value . '~';
	}
	
	public function getProp($name)
	{
		if (array_key_exists($name, $this->db)) {
			return $this->db[$name];
		}
		return '---';
	}
}

$a = new B();
$c = new C();

logHeader("\$a->x1 or \$a['x1']");
logRes('x1', $a, NULL);
logResFunc("\$a['x1']", $a['x1'], NULL);
logResDump("isset(\$a->x1)", var_export(isset($a->x1), true), '0(false)');
logResDump("isset(\$a['x1'])", var_export(isset($a['x1']), true), '0(false)', true);

logHeader("setPropertyMapping('x1', 'getProp', 'setProp')");
$a->setPropertyMapping('x1', 'getProp', 'setProp');
logRes('x1', $a, 'n/a');
logResFunc("\$a['x1']", $a['x1'], 'n/a');
logResDump("isset(\$a->x1)", var_export(isset($a->x1), true), '1(true)');
logResDump("isset(\$a['x1'])", var_export(isset($a['x1']), true), '1(true)', true);

logHeader("\$a['x1'] = 'foo'");
$a['x1'] = 'foo';
logRes('x1', $a, 'prefix_foo');
logResFunc("\$a['x1']", $a['x1'], 'prefix_foo');
logResDump("isset(\$a->x1)", var_export(isset($a->x1), true), '1(true)');
logResDump("isset(\$a['x1'])", var_export(isset($a['x1']), true), '1(true)', true);

logHeader("\$a['x2'] = 'bar'");
$a['x2'] = 'bar';
logRes('x2', $a, 'bar');
logResFunc("\$a['x2']", $a['x2'], 'bar');
logResDump("isset(\$a->x2)", var_export(isset($a->x2), true), '1(true)');
logResDump("isset(\$a['x2'])", var_export(isset($a['x2']), true), '1(true)', true);

logHeader("foreach (\$a as \$key => \$val)");
foreach ($a as $key => $val) {
	logRemark("\$a['$key']=$val");
}

?>
