<?php

include_once '../testClassSetterArray.php';

class C
{
	protected $db = array();
	
	public function setProp($name, $value)
	{
		$this->db[$name] = '~' . $value . '~';
	}
	
	public function getProp($name)
	{
		if (array_key_exists($name, $this->db)) {
			return $this->db[$name];
		}
		return '---';
	}
}

$a = new B();
$c = new C();

// todo: $a[]=xx and $a[123]=xx
// todo: isset($a[123]) and unset($a[123])


?>
