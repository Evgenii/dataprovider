<?php

use \spc\ProviderData as pd;

include_once '../testClassSetterArray.php';

class BB extends B {
	protected function getHeader($method)
	{
		$ts = microtime(true);
		return '[' . date("d.m.Y H:i:s", $ts) . " $ts] [$method]";
	}
	
	public function getClosure($t)
	{
		if ($this->getContainerAgent()->_enable_closure) {
			return	function (pd\Negotiator $negotiator) use ($t) {
					$oldResult = $negotiator->getResult();
					$result = '~n/a~';
					$arguments = $negotiator->getArguments();
					$oldArguments = $negotiator->getOldArguments();
					$nameVar = $arguments[0];
					if (is_numeric($nameVar)) {
						$result = $t->getVarFromStore($nameVar);
					} elseif (array_key_exists($nameVar, $this->_datas)) {
						$result = $this->_datas[$nameVar];
					}
					$arguments[0] = 'AAASD';
					$negotiator->setResult($result);
					echo $this->getHeader('GET') . sprintf(" Around-logging function '%s', name var '%s', getting '%s', return new '%s'", $negotiator->getName(), $nameVar, $oldResult, $result) . PHP_EOL;
					return true;
				};
		} else {
			$base = $this;
			$header = $this->getHeader('GET');
			return	function (pd\Negotiator $negotiator) use ($t, $base, $header) {
					$oldResult = $negotiator->getResult();
					$result = '~n/a~';
					$arguments = $negotiator->getArguments();
					$oldArguments = $negotiator->getOldArguments();
					$nameVar = $arguments[0];
					if (is_numeric($nameVar)) {
						$result = $t->getVarFromStore($nameVar);
					} else {
						$tmp = $base->getVarFromDatas($nameVar);
						if (!is_null($tmp)) {
							$result = $tmp;
						}
					}
					$negotiator->setResult($result);
					echo $header . sprintf(" Around-logging function '%s', name var '%s', getting '%s', return new '%s'", $nameFunc, $nameVar, $oldResult, $result) . PHP_EOL;
					return true;
				};
		}
	}
}
