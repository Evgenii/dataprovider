<?php

$numTest = 1;

function logRes ($name, $obj, $must, $newLine = false)
{
	$var = $obj->$name;
	$result = ($var === $must) ? '(OK)' : "(ERROR) // $must";
	echo sprintf("%s=%-24s %s", $name, $var, $result) . PHP_EOL;
	if ($newLine) echo PHP_EOL;
}

function logResDump ($name, $dump, $must, $newLine = false)
{
	echo sprintf("%s\n%s=%s", "// $must", $name, $dump) . PHP_EOL;
	if ($newLine) echo PHP_EOL;
}

function logResFunc ($textCall, $resultFromFunction, $must, $newLine = false)
{
	$result = ($resultFromFunction === $must) ? '(OK)' : "(ERROR) // $must";
	echo sprintf("%s=%-24s %s", $textCall, $resultFromFunction, $result) . PHP_EOL;
	if ($newLine) echo PHP_EOL;
}

function logHeader ($title, $newLine = false)
{
	global $numTest;
	echo sprintf(" *** [Test %s] %s", $numTest, $title) . PHP_EOL;
	if ($newLine) echo PHP_EOL;
	$numTest++;
}

function logRemark ($title, $newLine = false)
{
	echo sprintf(" --- %s", $title) . PHP_EOL;
	if ($newLine) echo PHP_EOL;
}
