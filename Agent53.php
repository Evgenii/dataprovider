<?php

namespace spc\ProviderData;

include_once 'Agent.php';

class Agent53 extends Agent
{
	
	public function setFunctionAspect($condition, $name = null, $callback = null, $assignWithThis = false)
	{
		return parent::setFunctionAspect($condition, $name, $callback);
	}
	
	public function setFunctionMapping($nameFunc, $callback = null, $assignWithThis = false)
	{
		return parent::setFunctionMapping($nameFunc, $callback);
	}
}