<?php

namespace spc\ProviderData;

class Negotiator implements \ArrayAccess, \Countable, \IteratorAggregate
{
	protected $name;
	protected $result;
	protected $arguments;
	protected $oldArguments;
	protected $_isResult = false;
	
	public function __construct($name, array &$arguments = array())
	{
		$this->name = $name;
		$this->result = NULL;
		$this->oldArguments = $arguments;
		$this->arguments = &$arguments;
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function &getResult()
	{
		return $this->result;
	}
	
	public function &getArguments()
	{
		return $this->arguments;
	}
	
	public function getOldArguments()
	{
		return $this->oldArguments;
	}
	
	public function setResult(&$result)
	{
		$this->_isResult = true;
		$this->result = &$result;
	}
	
	public function isResult()
	{
		return ($this->_isResult || !is_null($this->result));
	}

	public function offsetSet($offset, $value)
	{
		if (is_null($offset)) {
			$this->arguments[] = &$value;
		} elseif (is_numeric($offset)) {
			if ($offset > count($this->arguments)) {
				$this->arguments[] = &$value;
			} else {
				$this->arguments[$offset] = &$value;
			}
		} elseif ($offset == 'result') {
			$this->setResult($value);
		}
	}
	
	public function offsetExists($offset)
	{
		return array_key_exists($offset, $this->arguments);
	}
	
	public function offsetUnset($offset)
	{
		unset($this->arguments[$offset]);
		if (is_numeric($offset)) {
			$count = count($this->arguments);
			if ($offset < $count) {
				for ($i = $offset; $i <= $count - 1; $i++) {
					$this->arguments[$i] = $this->arguments[$i+1];
				}
				unset($this->arguments[$count]);
				ksort($this->arguments);
			}
		}
	}
	
	public function offsetGet($offset)
	{
		if (array_key_exists($offset, $this->arguments)) {
			return $this->arguments[$offset];
		} elseif ($offset == 'result') {
			return $this->result;
		}
		return null;
	}
	
	public function count()
	{
		return count($this->arguments);
	}
	
	public function getIterator() {
		return new \ArrayIterator($this->arguments);
	}
}
