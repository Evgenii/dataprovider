<?php

namespace spc\ProviderData;

include_once 'Negotiator.php';

class AgentAbstract
{
	const _FUNC_PRE = 'pre';
	const _FUNC_AROUND = 'around';
	const _FUNC_POST = 'post';
	const _TRIGGER_PRE = 'tr_pre';
	const _TRIGGER_POST = 'tr_post';
	const _HANDLER_NAME_DEFAULT = '_999_';
	
	public $_aspect_functions = array();
	public $_mapping_functions = array();
	public $_declared_functions = array();
	public $_obj_class = null;
	public $_ref_class = null;
	public $_enable_aspect = false;
	public $_enable_closure = false;
	public $_aspects = array (
			self::_FUNC_AROUND,
			self::_FUNC_POST,
			self::_FUNC_PRE,
			self::_TRIGGER_POST,
			self::_TRIGGER_PRE
		);
	public $_export_func = array(
				'setFunctionMapping',
				'setFunctionAspect',
				'getContainerAgent',
				'_execute'
			);
	
	public function  __construct($obj)
	{
		$this->_enable_closure = (version_compare(PHP_VERSION, '5.4.0') >= 0) ? true : false;
		$this->_obj_class = $obj;
		$this->_ref_class = new \ReflectionClass($obj);
		$methods = $this->_ref_class->getMethods();
		foreach ($methods as $method) {
			$methodName = $method->getName();
			if ('__' != substr($methodName, 0, 2)) {
				if ($method->isPrivate() || $method->isProtected()) {
					$method->setAccessible(true);
				}
				$this->_declared_functions[$methodName] = $method;
/*
				if ($method->isPublic()) {
					$this->_public_functions[$methodName] = $method;
				} else {
					$class = ($method->isStatic()) ? get_class($obj) : $obj;
					$this->_mapping_functions[$methodName] = array($class, $methodName);
				}
*/
			}
		}
		foreach ($this->_aspects as $aspect) {
			$this->_aspect_functions[$aspect] = array();
		}
	}
	
	public function getContainerAgent()
	{
		return $this;
	}
	
	public function setAspectDispatcher($enabled = true)
	{
		$this->_enable_aspect = (bool) $enabled;
	}
	
// ok
	public function setFunctionAspect($condition, $name = null, $callback = null, $assignWithThis = false)
	{
		if (is_null($name) && in_array($condition, $this->_aspects)) {
			$name = self::_HANDLER_NAME_DEFAULT;
		}
		if (empty($name) || !is_string($name) || ('__' == substr($name, 0, 2))) {
			return false;
		}
		if (!array_key_exists($condition, $this->_aspect_functions)) {
			return false;
		}
		if (is_string($callback)) {
			$callback = array($this->_obj_class, $callback);
		}
		$this->_enable_aspect = true;
		if ($assignWithThis && ($callback instanceof \Closure)) {
			$this->_aspect_functions[$condition][$name] = $callback->bindTo($this->_obj_class, get_class($this->_obj_class));
			return true;
		} elseif (is_callable($callback)) {
			$this->_aspect_functions[$condition][$name] = $callback;
			return true;
		} elseif (is_null($callback)) {
			unset($this->_aspect_functions[$condition][$name]);
			return true;
		}
		return false;
	}
	
	public function unsetAllAspectByName($name)
	{
		if (!is_string($name)) {
			return false;
		}
		foreach ($this->_aspects as $aspect) {
			unset($this->_aspect_functions[$aspect][$name]);
		}
		return true;
	}
	
// ok
	public function setFunctionMapping($nameFunc, $callback = null, $assignWithThis = false)
	{
		if (empty($nameFunc) || !is_string($nameFunc) || ('__' == substr($nameFunc, 0, 2))) {
			return false;
		}
		if (is_string($callback)) {
			$callback = array($this->_obj_class, $callback);
		}
		if ($assignWithThis && ($callback instanceof \Closure)) {
			if ($this->_enable_closure) {
				$this->_mapping_functions[$nameFunc] = $callback->bindTo($this->_obj_class, get_class($this->_obj_class));
			} else {
				$this->_mapping_functions[$nameFunc] = $callback;
			}
		} elseif (is_callable($callback)) {
			$this->_mapping_functions[$nameFunc] = $callback;
		} elseif (is_null($callback)) {
			unset($this->_mapping_functions[$nameFunc]);
		} else {
			return false;
		}
		return true;
	}
	
	protected function _getClosureByCallback($callback)
	{
		if ($callback instanceof \Closure) {
			return $callback;
		}
		if ($callback instanceof \ReflectionMethod) {
			if ($this->_enable_closure) {
				if ($callback->isStatic()) {
					return $callback->getClosure();
				}
				return $callback->getClosure($this->_obj_class);
			}
			$func = function() use ($callback) {
					$args = func_get_args();
					return $callback->invokeArgs($this->_obj_class, $args);
				};
			return $func;
		}
		if (is_string($callback)) {
			$callback = array($this->_obj_class, $callback);
		}
// todo: for Reflection method
		$func = function() use ($callback) {
				$args = func_get_args();
				return call_user_func_array($callback, $args);
			};
		if ($this->_enable_closure) {
			if (is_array($callback) && is_object($callback[0])) {
				return $func->bindTo($callback[0], get_class($callback[0]));
			}
			if (is_object($callback)) {
				return $func->bindTo($callback, get_class($callback));
			}
		}
// remark: don't work for private(protected) methods in PHP < 5.4.0
		return $func;
	}
	
	public function getFunctionMapping($nameFunc, $native = false)
	{
		if (is_string($nameFunc) && array_key_exists($nameFunc, $this->_mapping_functions)) {
			if ($native) {
				return $this->_mapping_functions[$nameFunc];
			}
			return $this->_getClosureByCallback($this->_mapping_functions[$nameFunc]);
		}
		return null;
	}
	
// todo:
	protected function _apply($condition, Negotiator $negotiator)
	{
		$name = $negotiator->getName();
		if (array_key_exists($name, $this->_aspect_functions[$condition])) {
			$callback = $this->_aspect_functions[$condition][$name];
			if (is_string($callback)) {
				$arguments = $negotiator->getArguments();
				$result = $this->_executeAspect(new Negotiator($callback, $arguments));
				$negotiator->setResult($result);
				return true;
			} elseif ($callback instanceof \ReflectionMethod) {
				return $callback->invoke($this->_obj_class, $negotiator);
			}
			return call_user_func($callback, $negotiator);
		}
		return true;
	}
	
// todo:
	protected function _applyAround($callback, Negotiator $negotiator)
	{
		$name = $negotiator->getName();
		if (array_key_exists($name, $this->_aspect_functions[self::_FUNC_AROUND])) {
			$callback = $this->_aspect_functions[self::_FUNC_AROUND][$name];
			if (is_string($callback)) {
				$arguments = $negotiator->getArguments();
				$result = $this->_executeAspect(new Negotiator($callback, $arguments));
				$negotiator->setResult($result);
				return true;
			} elseif ($callback instanceof \ReflectionMethod) {
				$callback->invoke($this->_obj_class, $negotiator);
			} else {
				call_user_func($callback, $negotiator);
			}
		} else {
			$arguments = $negotiator->getArguments();
			if ($callback instanceof \ReflectionMethod) {
				$result = $callback->invokeArgs($this->_obj_class, $arguments);
			} else {
				$result = call_user_func_array($callback, $arguments);
			}
			$negotiator->setResult($result);
		}
		return true;
	}
	
// todo:
	protected function _executeAspect(Negotiator $negotiator)
	{
		$name = $negotiator->getName();
		if (array_key_exists($name, $this->_mapping_functions)) {
			$callback = $this->_mapping_functions[$name];
		} elseif (array_key_exists($name, $this->_declared_functions)) {
			$callback = $this->_declared_functions[$name];
		} else {
			return null;
		}
		$this->_apply(self::_FUNC_PRE, $negotiator);
		$this->_applyAround($callback, $negotiator);
		$this->_apply(self::_FUNC_POST, $negotiator);
		return $negotiator->getResult();
	}
	
// todo:
	public function _execute($name, array &$arguments = array(), $filter = true)
	{
		if ($filter) {
			return $this->_executeAspect(new Negotiator($name, $arguments));
		}
		if (array_key_exists($name, $this->_mapping_functions)) {
			$callback = $this->_mapping_functions[$name];
			return call_user_func_array($callback, $arguments);
		} elseif (array_key_exists($name, $this->_declared_functions)) {
			$method = $this->_declared_functions[$name];
			return $method->invokeArgs($this->_obj_class, $arguments);
		}
		return null;
	}
}