<?php

namespace spc\ProviderData;

include_once 'AgentAbstract.php';

class Agent extends AgentAbstract
{
	public $_mapping_properties = array();
	public $_mapping_properties_set = array();
	public $_declared_properties_public = array();
	public $_declared_properties_static = array();
	
	public function  __construct($obj)
	{
		parent::__construct($obj);
		$this->_export_func = array_merge($this->_export_func,
				array(
					'setPropertyMapping',
					'setPropertyMappingGet',
					'setPropertyMappingSet',
				)
			);
		$properties = $this->_ref_class->getProperties(\ReflectionProperty::IS_PUBLIC + \ReflectionProperty::IS_STATIC);
		foreach ($properties as $property) {
			if ($property->isPublic()) {
				$this->_declared_properties_public[$property->getName()] = $property;
			}
			if ($property->isStatic()) {
				$this->_declared_properties_static[$property->getName()] = $property;
			}
		}
	}
	
// ok
	protected function _setPropertyMapping($nameProp, $callback, array &$store, $verifyIsset = true)
	{
		if (is_null($nameProp) || is_numeric($nameProp)) {
			$nameProp = self::_HANDLER_NAME_DEFAULT;
		}
		if (!is_string($nameProp) || $this->_ref_class->hasProperty($nameProp)) {
			return false;
		}
		if (is_null($callback)) {
			unset($store[$nameProp]);
			if ($verifyIsset && !$this->_obj_class->__isset($nameProp)) {
				$this->unsetAllAspectByName($nameProp);
			}
			return true;
		}
		if (is_string($callback)) {
			$callback = array($this->_obj_class, $callback);
		}
		if (is_callable($callback)) {
			$store[$nameProp] = $callback;
		} else {
			return false;
		}
		return true;
	}
	
// ok
	public function setPropertyMapping($nameProp = null, $callback = null, $callbackSet = null)
	{
		$result = $this->_setPropertyMapping($nameProp, $callback, $this->_mapping_properties, false);
		return ($this->_setPropertyMapping($nameProp, $callbackSet, $this->_mapping_properties_set)) ? $result : false;
	}
	
// ok
	public function setPropertyMappingGet($nameProp = null, $callback = null)
	{
		return $this->_setPropertyMapping($nameProp, $callback, $this->_mapping_properties);
	}
	
// ok
	public function setPropertyMappingSet($nameProp = null, $callback = null)
	{
		return $this->_setPropertyMapping($nameProp, $callback, $this->_mapping_properties_set);
	}
	
	public function getPropertyMappingGet($nameProp, $native = false)
	{
		if (is_string($nameProp) && array_key_exists($nameProp, $this->_mapping_properties)) {
			if ($native) {
				return $this->_mapping_properties[$nameProp];
			}
			return $this->_getClosureByCallback($this->_mapping_properties[$nameProp]);
		}
		return null;
	}
	
	public function getPropertyMappingSet($nameProp, $native = false)
	{
		if (is_string($nameProp) && array_key_exists($nameProp, $this->_mapping_properties_set)) {
			if ($native) {
				return $this->_mapping_properties_set[$nameProp];
			}
			return $this->_getClosureByCallback($this->_mapping_properties_set[$nameProp]);
		}
		return null;
	}
	
// ok
	protected function _applyTrigger($condition, $name, $value = null)
	{
		$handler = (is_null($name) || is_numeric($name)) ? self::_HANDLER_NAME_DEFAULT : $name;
		if (array_key_exists($handler, $this->_aspect_functions[$condition])) {
			$callback = $this->_aspect_functions[$condition][$handler];
			if (is_string($callback)) {
				return $this->_execute($callback, array($name, $value), true);
			} elseif ($callback instanceof \ReflectionMethod) {
				return $callback->invokeArgs($this->_obj_class, array($name, $value));
			} else {
				return call_user_func_array($callback, array($name, $value));
			}
		}
		return $value;
	}
	
// ok
	public function _applyBatch($condition, array $data)
	{
		if (!in_array($condition, $this->_aspects)) {
			return $data;
		}
		$result = array();
		foreach ($data as $key => $value) {
			$result[$key] = $this->_applyTrigger($condition, $key, $value);
		}
		return $result;
	}
	
// ok
	public function _propertySetValue($name, $value, $callback, $filter = true)
	{
		if (is_callable($callback)) {
			if ($filter) {
				$valueNew = $this->_applyTrigger(self::_TRIGGER_PRE, $name, $value);
				call_user_func_array($callback, array($valueNew, $name));
				return;
			}
			call_user_func_array($callback, array($value, $name));
		}
	}
	
// ok
	public function _propertySetValueViaRefl($name, $value, $filter = true)
	{
		$refl = $this->_declared_properties_public[$name];
		if ($filter) {
			$valueNew = $this->_applyTrigger(self::_TRIGGER_PRE, $name, $value);
			$refl->setValue($this->_obj_class, $valueNew);
			return;
		}
		$refl->setValue($this->_obj_class, $value);
	}
	
// ok
	public function _propertyGetValue($name, $callback, $filter = true)
	{
		if (is_callable($callback)) {
			if ($filter) {
				$value = call_user_func($callback, $name);
				return $this->_applyTrigger(self::_TRIGGER_POST, $name, $value);
			}
			return call_user_func($callback, $name);
		}
		return null;
	}
	
// ok
	public function _propertyGetValueViaRefl($name, $filter = true)
	{
		$refl = $this->_declared_properties_public[$name];
		if ($filter) {
			$value = $refl->getValue($this->_obj_class);
			return $this->_applyTrigger(self::_TRIGGER_POST, $name, $value);
		}
		return $refl->getValue($this->_obj_class);
	}
}