<?php

namespace spc\ProviderData;

include_once 'AgentAbstract.php';

class AgentStatic extends AgentAbstract
{
	
	public function setFunctionAspect($condition, $name = null, $callback = null, $assignWithThis = false)
	{
		return parent::setFunctionAspect($condition, $name, $callback);
	}
	
	public function setFunctionMapping($nameFunc, $callback = null, $assignWithThis = false)
	{
		return parent::setFunctionMapping($nameFunc, $callback);
	}
}