<?php

namespace spc\ProviderData;

include_once 'Agent.php';
include_once 'Agent53.php';
include_once 'AgentStatic.php';

class Factory
{
	static $_storeObj;
	static $_storeClass;
	
	protected static function getStorage()
	{
		if (is_null(self::$_storeObj)) {
			self::$_storeObj = new \SplObjectStorage();
		}
		return self::$_storeObj;
	}
	
	public static function getInstanceAgent(Container $container)
	{
		$store = self::getStorage();
		if ($store->contains($container)) {
			return $store->offsetGet($container);
		}
		if (version_compare(PHP_VERSION, '5.4.0') >= 0) {
			$agent = new Agent($container);
		} else {
			$agent = new Agent53($container);
		}
		$store->attach($container, $agent);
		return $agent;
	}
	
	public static function getInstanceAgentForStatic($container)
	{
		if (!is_string($container) || !class_exists($container)) {
			return null;
		}
		if (!is_array(self::$_storeClass)) {
			self::$_storeClass = array();
		}
		if (array_key_exists($container, self::$_storeClass)) {
			return self::$_storeClass[$container];
		}
		$agent = new AgentStatic($container);
		self::$_storeClass[$container] = $agent;
		return $agent;
	}
}