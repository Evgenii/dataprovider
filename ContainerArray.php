<?php

namespace spc\ProviderData;

include_once 'Container.php';

abstract class ContainerArray extends Container implements \ArrayAccess, \Countable, \IteratorAggregate
{
	public function  __construct(&$data = null)
	{
		parent::__construct($data);
		$this->resetContainer();
	}
	
	public function offsetContainer(&$arrayForContainer)
	{
		$this->_container = &$arrayForContainer;
	}
	
	public function resetContainer()
	{
		$this->_container = &$this->_datas;
	}

	public function offsetSet($offset, $value)
	{
		if (is_null($offset)) {
			if ($this->_agent_class->_enable_aspect) {
// remark: for PHP < 5.4.0 don't work aspect Container[]='something'
				$callback = function ($valueProperty, $offset) {
					$this->_container[] = $valueProperty;
				};
				$this->_agent_class->_propertySetValue(null, $value, $callback, true);
			} else {
				$this->_container[] = $value;
			}
		} else {
			$this->__propertySet($offset, $value, $this->_agent_class->_enable_aspect);
		}
	}
	
	public function offsetExists($offset)
	{
		return (isset($this->_container[$offset])
				|| array_key_exists($offset, $this->_agent_class->_mapping_properties)
				|| array_key_exists($offset, $this->_agent_class->_mapping_properties_set)
				|| $this->_agent_class->_ref_class->hasProperty($offset)
			);
	}
	
	public function offsetUnset($offset)
	{
		unset($this->_agent_class->_mapping_properties[$offset]);
		unset($this->_agent_class->_mapping_properties_set[$offset]);
		unset($this->_container[$offset]);
		$this->_agent_class->unsetAllAspectByName($offset);
	}
	
	public function offsetGet($offset)
	{
		return $this->__propertyGet($offset, $this->_agent_class->_enable_aspect);
	}
	
	public function count()
	{
		return count($this->_container + array_merge($this->_agent_class->_mapping_properties, $this->_agent_class->_mapping_properties_set));
	}
	
	public function getIterator() {
		$arr = array();
		$intersect = array_intersect_key($this->_container, $this->_agent_class->_mapping_properties);
		if ($this->_agent_class->_enable_aspect) {
			foreach ($intersect as $key => $callback) {
				$arr[$key] = $this->_agent_class->_propertyGetValue($key, $callback, true);
			}
			$container = array_diff_key($this->_container, $arr);
			$result = $arr + $this->_agent_class->_applyBatch(Agent::_TRIGGER_POST, $container);
		} else {
			foreach ($intersect as $key => $callback) {
				$arr[$key] = call_user_func($callback, $key);
			}
			$result = $arr + array_diff_key($this->_container, $arr);
		}
		return new \ArrayIterator($result);
	}
}