<?php

namespace spc\ProviderData;

include_once 'Factory.php';

abstract class Container
{
	const _FUNC_PRE = Agent::_FUNC_PRE;
	const _FUNC_AROUND = Agent::_FUNC_AROUND;
	const _FUNC_POST = Agent::_FUNC_POST;
	const _TRIGGER_PRE = Agent::_TRIGGER_PRE;
	const _TRIGGER_POST = Agent::_TRIGGER_POST;
	
	protected $_datas = array();
	protected $_container = null;
	protected $_agent_class = null;
	
	public function  __construct(&$data = null)
	{
		$this->_agent_class = Factory::getInstanceAgent($this);
		if (!is_null($data) && (is_array($data) || $data instanceof \Traversable)) {
			$this->_datas = &$data;
		}
		$this->_container = &$this->_datas;
	}
	
	public function __call($name, $arguments)
	{
		if (in_array($name, $this->_agent_class->_export_func)) {
			return call_user_func_array(array($this->_agent_class, $name), $arguments);
		}
		return $this->_agent_class->_execute($name, $arguments, $this->_agent_class->_enable_aspect);
	}
	
// todo:
	public static function __callStatic($name, $arguments)
	{
		$agent = Factory::getInstanceAgentForStatic(get_called_class());
		if (in_array($name, $agent->_export_func)) {
			return call_user_func_array(array($agent, $name), $arguments);
		}
		return $agent->_execute($name, $arguments, $agent->_enable_aspect);
	}
	
// todo: apply ASPECT trigger for all types
	public function __get($name)
	{
		return $this->__propertyGet($name, $this->_agent_class->_enable_aspect);
	}
	
	public function __set($name, $value)
	{
		$this->__propertySet($name, $value, $this->_agent_class->_enable_aspect);
	}
	
	public function __isset($name)
	{
		return (isset($this->_container[$name])
				|| array_key_exists($name, $this->_agent_class->_mapping_properties)
				|| array_key_exists($name, $this->_agent_class->_mapping_properties_set)
				|| array_key_exists($name, $this->_agent_class->_declared_properties_public)
			);
	}
	
	public function __unset($name)
	{
		unset($this->_agent_class->_mapping_properties[$name]);
		unset($this->_agent_class->_mapping_properties_set[$name]);
		unset($this->_container[$name]);
		$this->_agent_class->unsetAllAspectByName($name);
	}
	
// todo
	protected function getCalledObject()
	{
		$tracks = debug_backtrace();
		foreach ($tracks as $stack) {
			if (isset($stack['object']) && ($stack['object'] !== $this)) {
				return $stack['object'];
			}
		}
		return null;
	}
	
// todo
	protected function getHistoryCalledObject($nameFunc, $object)
	{
		$tracks = debug_backtrace();
		foreach ($tracks as $stack) {
			if (isset($stack['function']) && ($stack['function'] == $nameFunc) && isset($stack['object']) && ($stack['object'] instanceof $object)) {
				return $stack['object'];
			}
		}
		return null;
	}
	
// ok
	public function __propertyGet($name, $filter = true)
	{
		if (array_key_exists($name, $this->_agent_class->_declared_properties_public)) {
			return $this->_agent_class->_propertyGetValueViaRefl($name, $filter);
		}
		$funcName = 'get' . ucfirst($name);
		if (array_key_exists($funcName, $this->_agent_class->_declared_functions)) {
			$method = $this->_agent_class->_declared_functions[$funcName];
			if (!$filter) {
				return $method->invoke($this, $name);
			}
			$callback = function ($nameProperty) use ($method) {
					return $method->invoke($this, $nameProperty);
				};
//			$callback = array($this, $funcName);
//			return $this->$funcName($name);
		} elseif (array_key_exists($name, $this->_agent_class->_mapping_properties)) {
			if (!$filter) {
				return call_user_func($this->_agent_class->_mapping_properties[$name], $name);
			}
			$callback = $this->_agent_class->_mapping_properties[$name];
		} elseif (array_key_exists($name, $this->_container)) {
			if (!$filter) {
				return $this->_container[$name];
			}
			if ($this->_agent_class->_enable_closure) {
				$callback = function ($nameProperty) {
						return $this->_container[$nameProperty];
					};
			} else {
				$_this = $this;
				$callback = function ($nameProperty) use ($_this) {
						return $_this->__propertyGet($nameProperty, false);
					};
			}
		} else {
			return null;
		}
		return $this->_agent_class->_propertyGetValue($name, $callback, true);
	}
	
	public function __propertySet($name, $value, $filter = true)
	{
		if (array_key_exists($name, $this->_agent_class->_declared_properties_public)) {
			$this->_agent_class->_propertySetValueViaRefl($name, $value, $filter);
			return;
		}
		$funcName = 'set' . ucfirst($name);
		if (array_key_exists($funcName, $this->_agent_class->_declared_functions)) {
			$method = $this->_agent_class->_declared_functions[$funcName];
			if (!$filter) {
				$method->invoke($this, $value, $name);
				return;
			}
			$callback = function ($valueProperty, $nameProperty) use ($method) {
					$method->invoke($this, $valueProperty, $nameProperty);
				};
//			$callback = $callback->bindTo($this, get_class($this));
//			$callback = array($this, $funcName);
		} elseif (array_key_exists($name, $this->_agent_class->_mapping_properties_set)) {
			$callback = $this->_agent_class->_mapping_properties_set[$name];
			if (!$filter) {
				call_user_func_array($callback, array($value, $name));
				return;
			}
		} else {
			if (!$filter) {
				$this->_container[$name] = $value;
				return;
			}
			if ($this->_agent_class->_enable_closure) {
				$callback = function ($valueProperty, $nameProperty) {
						$this->_container[$nameProperty] = $valueProperty;
					};
//				$callback = $callback->bindTo($this, get_class($this));
			} else {
				$_this = $this;
				$callback = function ($valueProperty, $nameProperty) use ($_this) {
						$_this->__propertySet($nameProperty, $valueProperty, false);
					};
			}
		}
//echo "dubug2=$name=$value" . PHP_EOL;
		$this->_agent_class->_propertySetValue($name, $value, $callback, $filter);
	}
}